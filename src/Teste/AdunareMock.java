package Teste;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;


import pachet.Calculator;

public class AdunareMock {
	Calculator calcObj;
	@Before
	/**
	 * Creare mock obj
	 */
	public void create(){
		calcObj= mock(Calculator.class); //Creare  mock Object
		when(calcObj.adunare(1, 2)).thenReturn(3); 
	}
	
	@Test
	public void test() {
		assertSame(3, calcObj.adunare(1,2)); // return 3. Corect=> orice pun in loc e gresit
	}

}
