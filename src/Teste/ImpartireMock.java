package Teste;


import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import pachet.Calculator;

public class ImpartireMock {
	Calculator calcObj;
	     
	@Before
	public void create(){
		calcObj= mock(Calculator.class); //Creare Calculator Object/ 
		when(calcObj.impartire(anyInt(), eq(0))).thenThrow(new ArithmeticException()); // impartitorul este 0. Altfel eroare!!!
	}
	
	@Test(expected=ArithmeticException.class) 
	public void test() {
		calcObj.impartire(3,0); 
	}

}
