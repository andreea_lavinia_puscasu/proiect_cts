package Teste;

import static org.junit.Assert.*;

import org.junit.Test;

import pachet.Calculator;

public class InmultiriTeste {
  
	@Test
	public void test() {
		assertEquals("10 x 0 trebuie sa fie 0", 0, Calculator.inmultire(10, 0));
		assertEquals("0 x 10 trebuie sa fie 0", 0, Calculator.inmultire(0, 10));
		assertEquals("0 x 0 trebuie sa fie 0",  0, Calculator.inmultire(0, 0));
		assertEquals("10 x 5trebuie sa dea 50", 50,Calculator.inmultire(10, 5)); 

	}

}
