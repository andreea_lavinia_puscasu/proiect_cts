package Teste;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pachet.Calculator;

public class CalculatorTest {
	Calculator calcObj;
	@Before
	public void create(){
		calcObj= new Calculator();
	}
	
	@Test
	public void testAdunare() {
		
		assertSame(3, calcObj.adunare(1,2));
	}
}
