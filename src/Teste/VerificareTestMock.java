package Teste;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import pachet.Calculator;

public class VerificareTestMock {
	Calculator calcObj;

	@Before
	public void create(){
		calcObj= mock(Calculator.class);
		when(calcObj.adunare(1,2)).thenReturn(3); 
	} 
	
	@Test
	public void test() {
		//functia cu 2 argum
		assertSame(calcObj.adunare(1,2),3);
		
		//se verifica daca se test pt 1,2
		verify(calcObj).adunare(eq(1), eq(2));
		
		verify(calcObj,times(1)).adunare(1,2);
	}


}
