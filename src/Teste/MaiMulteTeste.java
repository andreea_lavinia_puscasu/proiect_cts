package Teste;

import static org.junit.Assert.*;

import org.junit.Test;
import pachet.Calculator;
public class MaiMulteTeste {

	//public boolean esteEgal(int a, int b)
	@Test
	public void testEsteEgal() {
		Calculator c = new Calculator();
		assertTrue(c.esteEgal(50, 50));
	}
	  
	
	//pt public boolean sumeEgale(int a, int suma2, int b,int suma1)
	@Test
	public void sumaEsteEgalaTest() {
		Calculator calc = new Calculator();
		assertTrue(calc.sumeEgale(10, 30, 20, 30));
	}
	
	//public boolean rezultatInmultiriEegale(int a, int inmultire1, int b,int inmultire2)
	@Test
	public void rezultatInmultiriEegaleTest()  {
		Calculator calc = new Calculator();
		assertTrue(calc.rezultatInmultiriEegale(4, 20, 5, 20));
	}
	
	//public boolean rezultatImpartiriEgal(int a, int b,int c)
	@Test
	public void rezultatImpartiriEgalTest()  {
		Calculator calc = new Calculator();
		assertTrue(calc.rezultatImpartiriEgal(8, 2, 4));
	}
	
	@Test
	public void rezultatDiferentaEgalTest() {
		Calculator calc = new Calculator();
		assertTrue(calc.rezultatDiferentaEgal(10, 5, 5, 5));
	}
}
