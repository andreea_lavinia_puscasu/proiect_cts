package Teste;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AdunareMock.class, AdunareTest.class, CalculatorTest.class,
		ConcatenateTest.class, CountTest.class, DiferentaTest.class,
		ImpartireMock.class, InmultireFaraMock.class, InmultiriTeste.class,
		MaiMulteTeste.class, NumarPrimMock.class, SquareTest.class,
		SuitCase.class, TestingSuite.class, VerificareTestMock.class })
public class TesteSuitFaraMaiMulteTeste { 

}
