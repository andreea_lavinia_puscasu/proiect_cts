package Teste;

import static org.junit.Assert.*;

import org.junit.Test;

import pachet.Calculator;

public class ConcatenateTest {

	@Test   
	public void test() {
		Calculator test = new Calculator();
		String result = test.concatenate("PROIECT_", "CTS");
		assertEquals("PROIECT_CTS",result);
	}

}
