package Teste;

import static org.junit.Assert.*;
import org.junit.Test;
import pachet.Calculator;
   

public class DiferentaTest {

	@Test
	public void test() {
		assertEquals("eroare la diferenta() ",  1, Calculator.diferenta(6, 5));
	    assertEquals("eroare la diferenta ()",  0, Calculator.diferenta(6, 6));
	    assertEquals("eroare la diferenta ()",  -1, Calculator.diferenta(2, 3));
	}
}

    