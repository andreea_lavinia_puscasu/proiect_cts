package Teste;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import pachet.Calculator;


public class TestarePreluareDateFisier {
	public FileReader reader;
	public BufferedReader bf;
	
	 
	@Before
	public void setUp() throws Exception {
		reader = new FileReader("date");
		bf = new BufferedReader(reader);
	}
	@After
	public void tearDown() throws Exception {
		bf.close();
		reader.close();
	
	}
	//testam citirea din fisier -> 
	@Test
	public void testPreluareFisier() throws ParseException {
		try {
			String linie = null;
			while((linie = bf.readLine())!= null && linie.length() != 0){
				int a = Integer.parseInt(linie.split(" ")[0]);
				int b = Integer.parseInt(linie.split(" ")[1]);
				
				Calculator calc = new Calculator();
				int rezultat = calc.adunare(a, b);
				System.out.println(a + " + " + b + " = " + rezultat);
				assertEquals(a+b, rezultat);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}